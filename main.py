from scipy.constants import m_e,electron_volt

from matplotlib import pyplot as plt
from Conductivity import Conductivity

Ge_properties = {
    'name':'Ge',
    'r': 2.0,#intrinsic semiconductor
    'Sigma_real':2.17,#1/0.46
    'T': 300,#K
    'mobility_e': 0.39, #m^2/V/s
    'Me_ef': 0.22 * m_e,
    'Vf_e': 3.1e5,#m/c
    'Eg': 0.661*electron_volt#J, T=300 K 
}

InAs_properties = {
    'name':'InAs',
    'r': 2.0,#intrinsic semiconductor
    'Sigma_real':625,#100/0.16
    'T': 300,#K
    'mobility_e': 4.0, #m^2/V/s
    'Me_ef': 0.023 * m_e,
    'Vf_e': 7.7e5,#m/c
    'Eg': 0.354*electron_volt#J, T=300 K 
}

Ge_conductivity = Conductivity(**Ge_properties)
Ge_conductivity.validate_model()
Mu_f, Ge_G_ball, Ge_G_diff = Ge_conductivity()

InAs_conductivity = Conductivity(**InAs_properties)
InAs_conductivity.validate_model()
Mu_f, InAs_G_ball, InAs_G_diff = InAs_conductivity()


Ge_G_ball, Ge_G_diff, InAs_G_ball, InAs_G_diff = Ge_G_ball*1e3, Ge_G_diff*1e3, InAs_G_ball*1e3, InAs_G_diff*1e3
max_y = max(Ge_G_ball.max(), Ge_G_diff.max(), InAs_G_ball.max(), InAs_G_diff.max())



fig, (ax1, ax2) = plt.subplots(1,2)
ax1.plot(Mu_f,Ge_G_ball,'k--',label='ballistic')
ax1.plot(Mu_f,Ge_G_diff,'k',label='diffusion')
ax1.set_title('Ge conductivity')

ax2.plot(Mu_f,InAs_G_ball,'k--',label='ballistic')
ax2.plot(Mu_f,InAs_G_diff,'k',label='diffusion')
ax2.set_title('InAs conductivity')
for ax in fig.get_axes():
    ax.set_ylim(bottom = 0,top = max_y)
    ax.set_xlim(left = -5,right = 5)
    ax.legend()
    ax.grid(True)
    ax.set(xlabel='\u03B7f', ylabel='G, mS')
# for ax in fig.get_axes():
#     ax.label_outer()
fig.set_size_inches(8,5)
#plt.show()
plt.savefig('Графік.png',dpi=400)