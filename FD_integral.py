from scipy import integrate
from scipy import constants
import numpy as np
from math import gamma

class Fermi_Dirac_integral:
    def __init__(self):
        self.F0 = lambda Mu_f: np.log(1+np.exp(Mu_f))
        self.F_minus_1 = lambda Mu_f: 1/(np.exp(-Mu_f)+1)

    def Fermi_function(self,Mu,j,Mu_f):
        if (Mu<Mu_f):
            y = np.power(Mu,j)/(np.exp(Mu-Mu_f)+1)
        else:
            y = np.power(Mu,j)*np.exp(-(Mu-Mu_f))/(1+np.exp(-(Mu-Mu_f)))
        return y

    def Fermi_Dirac_integral(self,j,Mu_f):
        integrated, error= integrate.quad(self.Fermi_function,0,np.inf,args=(j,Mu_f))
        Integral = 1/(gamma(j+1))*integrated
        return Integral

    def FivePoint_FirstDerivative(self,j,x,dx=0.01):
        f = lambda Mu_f: self.Fermi_Dirac_integral(j,Mu_f)
        y = (-f(x+2*dx)+8*f(x+dx)-8*f(x-dx)+f(x-2*dx))/(12*dx)
        return y

    def __call__(self,j,Mu_f):
        if (j>0):
            Integral = self.Fermi_Dirac_integral(j,Mu_f)
        elif (j==0):
            Integral = self.F0(Mu_f)
        elif (j==-1):
            Integral = self.F_minus_1(Mu_f)
        elif (-1<j<0):
            Integral = self.FivePoint_FirstDerivative(j+1,Mu_f)
        else:
            raise ValueError("Fermi–Dirac integral does not converge for j <-1")
        return Integral


if (__name__ == '__main__'):
    real = 20.9145

    FDI = Fermi_Dirac_integral()


    print("straight error:",1-real/FDI.FivePoint_FirstDerivative(5/2, 5))
    print("derivative error",1-real/FDI(3/2,5))

