from scipy.constants import e,h,k,hbar
from math import gamma,pi
import numpy as np
from FD_integral import Fermi_Dirac_integral

L = 1 * 10**(-6)#m<-mcm
W = 200 * 10**(-9)#m
A = 200 * 200 * 10**(-18)#m^2
q = e

class Conductivity:
    def __init__(self,**kwargs):
        self.FDI = Fermi_Dirac_integral()
        self.name=kwargs['name']
        self.r = kwargs['r']
        self.Sigma_real = kwargs['Sigma_real']
        self.T = kwargs['T']
        self.M_ef = kwargs['Me_ef']
        self.Eg = kwargs['Eg']
        self.Lambda = (kwargs['mobility_e'] * kwargs['Me_ef'] * kwargs['Vf_e']) / e
        #self.Lambda_h = (kwargs['mobility_h'] * kwargs['Mh_ef'] * kwargs['Vf_h']) / e


        self.G_ballistic_1D = np.vectorize(lambda Mu_f: (2*np.power(q,2)/h)*self.FDI(-1,Mu_f))
        self.G_diffusion_1D = np.vectorize(lambda Mu_f: (2*np.power(q,2)/h)*(self.Lambda/L)*gamma(self.r+1)*self.FDI(self.r-1,Mu_f))

        self.G_diffusion_2D = np.vectorize(lambda Mu_f: W*(2*np.power(q,2)/h)*(self.Lambda/L)*      \
                                (np.power(2*self.M_ef*k*self.T,1/2)/(pi*h))*gamma(self.r+3/2)*self.FDI(self.r-1/2,Mu_f))
        self.G_diffusion_3D = np.vectorize(lambda Mu_f: A*(2*np.power(q,2)/h)*(self.Lambda/L)*      \
                                (self.M_ef*k*self.T/(2*pi*np.power(hbar,2)))*gamma(self.r+2)*self.FDI(self.r,Mu_f))


    def validate_model(self):
        Mu_f = -(self.Eg/2)/(k*self.T)
        print(self.name)
        # Sigma = self.G_diffusion_1D(Mu_f)*L
        # print("1D. Calculated: {0}, Table value: {1},Difference: {2}".format(Sigma, self.Sigma_real, Sigma/self.Sigma_real))
        # Sigma = self.G_diffusion_2D(Mu_f)*L/W
        # print("2D. Calculated: {0}, Table value: {1},Difference: {2}".format(Sigma, self.Sigma_real, Sigma/self.Sigma_real))
        Sigma = self.G_diffusion_3D(Mu_f)*L/A
        print("3D. Calculated: {0}, Table value: {1},Difference: {2}".format(Sigma, self.Sigma_real, Sigma/self.Sigma_real))


    def __call__(self,steps=101):
        Mu_f = np.linspace(-5.0, 5.0, steps)
        G_ballistic = self.G_ballistic_1D(Mu_f)
        G_diffusion = self.G_diffusion_1D(Mu_f)
        return Mu_f, G_ballistic, G_diffusion