from scipy import integrate
import numpy as np

f = np.exp

real = np.exp(20) - np.exp(0)
quad, error = integrate.quad(f,0,20)
fixed_quad, error = integrate.fixed_quad(f,0,20)
quadrature, error = integrate.quadrature(f,0,20)

print("quad error:",1-real/quad)
print("fixed_quad error:",1-real/fixed_quad)
print("quadrature error:",1-real/quadrature)