import numpy as np

def three_point(f,x,dx):
    y = (f(x+dx)-f(x-dx))/(2*dx)
    return y

def FivePoint_FirstDerivative(f,x,dx):
    y = (-f(x+2*dx)+8*f(x+dx)-8*f(x-dx)+f(x-2*dx))/(12*dx)
    return y


function  = np.exp
derivative = np.exp

x=3
dx=0.01

print("real:",derivative(x))
print("3 point error:",(1-three_point(function,x,dx)/derivative(x))*100)
print("5 point error:",(1-FivePoint_FirstDerivative(function,x,dx)/derivative(x))*100)